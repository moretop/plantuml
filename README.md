# plantuml
编辑器地址 [https://moretop.gitee.io/plantuml](https://moretop.gitee.io/plantuml)

#### 介绍
![uml](http://www.plantuml.com/plantuml/svg/NLHRRzCm57xFhmZY0UXKyHUu31xGUE4Ya01DRckrLXEdSXnGtxeRsrX7W7Mbx586RAmIMxiXrh6bQ_zCR6TF-miuTPgkA59qlk_u71-VsosR0aBVf6pj2ir14-evaCv3ejbvX0k005DFII9BB0Tdxbg6HVIhz--6dwQDlRXgvuifmu6lz4IYO01C7TD8mWoY5aaaNk94GZIMsAdBFjRylokYqX1hiqyVJeYAnzQxx7Y7pHqd4jhOcEvxFyJM5EkuIYMfiKcTLrgyF7LUQOhr6dS_iRaTiJZFcqTAw_UQl7B2goT-jpTWDjb-JJ4NdGNcjdYbopOQoZclpljUs_-xVz5P3AzassjYhHbCRulkUrQkQibaSb879ie29QOC-Una7gDiZWuO1sO9A0vHWI0SATEGE0O4U1GbBOSQ2CE89JG9p9HCRWop1CA1JJ5baUnbUTGwgajVDiAeXh6OW4A2m0Y8ZVkvCI45HYGY4rhIjf0RO4oBIfGbH5Y91DgE2PFzYnHZ0vongJNmQ2FZjGn7oNb8qxdY21Yztow0D3H0Aa9E0P8S9DP_SDJ6mOZ2pBL-iTmM-r3cdzl2gx65jgmTwso5ZH8f5HsqvyAg7dgilgIfP4YJiF_aoJwKBUXtbuRmq8jbVZGLDckiycPbkmpXkpOlJSMGjKw2msynF9zfYBFVGsrpBRnNXISV8avx_DlRiENFlelLyldczwZ1wsrUdutxFTYT3Sepx5EPksxGEmXQ-q6Zf5z_F3vnGnqOHnG0FOE0h9uPkQxKnEB9S6HAcyBxmrgTc7bcaRn-Xq2GprXliFwO4YTD7G97tVZUZdxRJYFq4D2SpXUg-eC9y0G-xqEnzvEvnt9q-Ue1s__9vlRae2gxUvWYMjGVoT9IP64xXmgnOys0MOWpSXQmR4ZjyYPWXtNfxl8QkEYKPGoYMMBBgz4owFqwBtrLXRsfXbsk3fdRkimjhtN5zgaq2kkzkwn6c6rNWzrfIOuiZfLb_smZ98_SO7U1VMdmZJssSI0wAt9PQ660AavDkoN_uHxy1m00)

#### 皮肤实例
[https://moretop.gitee.io/plantuml/themes](https://moretop.gitee.io/plantuml/themes)

#### 资源
plantuml官网 [https://plantuml.com/zh](https://plantuml.com/zh)

#### 键盘操作
- 按 **`F2`** 进入键盘操作模式

- 按 **`↑`** **`↓`** 方向键选择

- 按 **`Enter`** 插入到编辑器



#### 支持绘制的UML
- 时序图
- 用例图
- 类图
- 活动图（流程图）
- 组件图
- 状态图
- 对象图
- 部署图
- 定时图

#### 同时还支持以下非UML图:
- 线框图形界面
- 架构图
- 规范和描述语言 (SDL)
- Ditaa diagram
- 甘特图
- 思维导图
- Work Breakdown Structure diagram
- 以 AsciiMath 或 JLaTeXMath 符号的数学公式
- Entity Relationship diagram





