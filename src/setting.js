var PLANTUML_SERVER = "http://www.plantuml.com/plantuml/svg/";
var PLANTUML_PREFIX = "```plantuml";
var PLANTUML_SUFFIX = "```";
var DEFAULT_THEME = "cerulean";

// 默认的Uml
var INIT_UML = "!theme " + DEFAULT_THEME + "\n'更多皮肤 https://moretop.gitee.io/plantuml/themes\nskinparam backgroundColor #FCFCFC\n\nstart\n:activity;\nstop";

// 流程图标片段
var SNIPPETS = [
  // { n: "ENTER", v: "\n" },
  { n: PLANTUML_PREFIX, v: PLANTUML_PREFIX + "\n^!\n" + PLANTUML_SUFFIX },
  { n: "@UML", v: "@startuml\n!theme " + DEFAULT_THEME + "\n^!\n@enduml" },
  { n: "BG#FCFCFC", v: "skinparam backgroundColor #FCFCFC" },
  // { n: "beautify", v: "skinparam backgroundColor #FCFCFC\n!include https://raw.githubusercontent.com/xuanye/plantuml-style-c4/master/core.puml\nGREEN_ARROW" },
  { n: "themes", v: "!theme " + DEFAULT_THEME + "\n'https://moretop.gitee.io/plantuml/themes" },
  { n: "scale", v: "scale 1.5" },
  { n: "start", v: "start\n" },
  { n: "stop", v: "stop\n" },
  { n: "end", v: "end" },
  { n: ":;", v: ":^!;\n",c: 1 },
  { n: ":/", v: ":^!/\n",c: 1 },
  { n: ":|", v: ":^!|\n",c: 1 },
  { n: "->;", v: "->;",c: 2 },
  { n: "()", v: "(^!)",c: 1 },
  { n: "[#]", v: "[#]",c: 2 },
  { n: "if", v: "if (?) then (Y)\n:;\nelse (N)\n:;\nendif" },
  { n: "elseif", v: "elseif (?) then (N)\nend" },
  { n: "while", v: "while (?) is (Y)\n:;\nendwhile (N)" },
  { n: "repeat", v: "repeat\n:;\nrepeatwhile (?) is (Y)" },
  { n: "detach", v: "detach\n" },
  { n: "note", v: "note right\n^!\nend note" },
  { n: "legend", v: "legend center\n^!\nend legend" },
  { n: "split", v: "split\n:;\nsplit again\n:;\nend split" },
  { n: "fork", v: "fork\n:;\nfork again\n:;\nend fork" },
  { n: "partition", v: "partition name {\n^!\n}" },
  { n: "swimlane1", v: "|Swimlane1|" },
  { n: "swimlane2", v: "|#antiquewhite|Swimlane2|" },
  { n: "form", v: "@startsalt\n{\n\"    \"\n[确定]\n}\n@endsalt" },
  { n: "tree", v: "@startsalt\n{\n\t{T\n\t\t+\n\t\t++\n\t}\n}\n@endsalt" },
  { n: "mindmap", v: "@startmindmap\n!theme " + DEFAULT_THEME + "\nskinparam backgroundColor #FCFCFC\n\n* root\n** node1\n***_  boxless\n** node2\n\n@endmindmap" },
  { n: "wbs", v: "@startwbs\n!theme " + DEFAULT_THEME + "\nskinparam backgroundColor #FCFCFC\n\n* root\n** node1\n***_  boxless\n** node2\n\n@endwbs" },
];

// 皮肤名
var THEMES = [
  "united",
  "spacelab",
  "bluegray",
  "hacker",
  "sandstone",
  "minty",
  "plain",
  "black-knight",
  "silver",
  "lightgray",
  "metal",
  "cerulean",
  "cerulean-outline",
  "cyborg",
  "cyborg-outline",
  "superhero",
  "superhero-outline",
  "materia",
  "materia-outline",
  "sketchy",
  "sketchy-outline",
  "resume-light",
  "mimeograph",
  "amiga",
  "blueprint",
  "crt-amber",
  "crt-green"
];